/**
 * 
 */
package fileProcessorDecorator.fileOperations;

import java.util.ArrayList;
import java.util.Collection;

import fileProcessorDecorator.util.FileDisplayInterface;
import fileProcessorDecorator.util.Results;


/**
 * @author prathamesh
 *
 */
public class ParagraphDecorator extends FileProcessorAbstractBase {
	private ArrayList<String> paraDecorator;
	private ArrayList<String> initStorage;
	private FileProcessorAbstractBase baseClass;
	private FileDisplayInterface results;
	public ParagraphDecorator(FileProcessorAbstractBase baseClassIn, FileDisplayInterface resIn) {
		// TODO Auto-generated constructor stub
		baseClass = baseClassIn;
		results = resIn;
	}
	
	@Override
	public Collection getDecorator(InputDetails ip) {

		// TODO Auto-generated method stub
		paraDecorator = ip.getResParagraph();
		initStorage = ip.getResInitialStorage();
		for(int i = 0; i < initStorage.size(); i++){
			String[] chopped = initStorage.get(i).split("\\.  +");
			int size = chopped.length;
			
			if(size == 1){
				paraDecorator.add(chopped[0]);
			}
			
			if(size > 1){
				for(int k = 0; k < chopped.length; k++){
					if(k == size - 1){
						paraDecorator.add(chopped[k]);
					}
					else{
						paraDecorator.add(chopped[k] + ".");
					}
				}
			}
			
		}
		 
			((Results)results).storeNewResult("---DECORATOR_ParagraphDecorator_START --- ") ;

		for(String s : paraDecorator){
			((Results)results).storeNewResult(s) ;
		}
		((Results)results).storeNewResult("---DECORATOR_ParagraphDecorator_END --- ") ;
		
		return paraDecorator;
	}
}