/**
 * 
 */
package fileProcessorDecorator.fileOperations;

import java.util.ArrayList;

/**
 * @author prathamesh
 *
 */
public class FileRead {
	private FileProcessor fp;
	private InputDetails ip;
	private ArrayList<String> fileRead;
	private String line; 
	
	/**
	 * @param inputFileIn, takes string InputFile 
	 * @param ipIn, InputDetails reference
	 */
	public FileRead(String inputFileIn, InputDetails ipIn){
		fp = new FileProcessor(inputFileIn);
		ip = ipIn;
	}
	
	/**
	 *Stores inputfile into ArrayList present 
	 *in InputDetails file 
	 */
	public void initialStorage(){
		fileRead = ip.getResInitialStorage();
		while((line = fp.readLine()) != null){
			fileRead.add(line);
		}
	}
}
