/**
 * 
 */
package fileProcessorDecorator.fileOperations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import fileProcessorDecorator.util.FileDisplayInterface;
import fileProcessorDecorator.util.Results;

import java.util.Set;

/**
 * @author prathamesh
 *
 */
public class WordFrequencyDecorator extends FileProcessorAbstractBase {
	private ArrayList<String> wordDecorator;
	private FileProcessorAbstractBase baseClass;
	private Map<String, Integer> map;
	private FileDisplayInterface results; 
	public WordFrequencyDecorator(FileProcessorAbstractBase baseClassIn, FileDisplayInterface resIn){
		baseClass = baseClassIn;
		results = resIn;
	}
	@Override
	public Collection getDecorator(InputDetails InputDetailsIn) {
		// TODO Auto-generated method stub
		wordDecorator = (ArrayList<String>) baseClass.getDecorator(InputDetailsIn);
		map = InputDetailsIn.getResWordFrequency();
		for(String s : wordDecorator){
			String[] chopped = s.split("\\s+");
			for(String temp : chopped){
				if(map.containsKey(temp)){
					Integer count = map.get(temp);
					map.put(temp, (count == null) ? 1 : count +1);
				}
				else{
					map.put(temp, 1);
				}
			}
		}
		

		((Results)results).storeNewResult("\n---DECORATOR_WordFrequencyDecorator_START --- ") ;
		Set<Entry<String, Integer>> hashSet = map.entrySet();
		for(Entry entry : hashSet){
			String key = entry.getKey() + "";
			String temp = ":";
			String value = entry.getValue() + "";
			key = key.format("%-15s", key);
			temp = temp.format("%5s", temp);
			((Results)results).storeNewResult(key + temp + value) ;

		}
		((Results)results).storeNewResult("\n---DECORATOR_WordFrequencyDecorator_END --- ") ;

		return null;
	}

}
