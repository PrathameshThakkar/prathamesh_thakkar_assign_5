/**
 * 
 */
package fileProcessorDecorator.fileOperations;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * @author prathamesh
 *
 */
public class FileProcessor {
	private  Scanner in = null;

	/**
	 * @param fileName file to be opened. 
	 * 
	 */
	public FileProcessor(String fileName){
		File inputFile = new File(fileName);
		try {
			in = new Scanner(inputFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.err.println("File not found");
			e.printStackTrace();
			System.exit(0);
		}
		finally{
			
		}
	}
	/**
	 * @return String, each line which is read
	 * 
	 */
	public String readLine(){
		String line = null;
		while(in.hasNextLine()){
			line = in.nextLine();
			return line;
		}
		line = null;
		in.close();
		return line;
	}
}
