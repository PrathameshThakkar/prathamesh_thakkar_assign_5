/**
 * 
 */
package fileProcessorDecorator.fileOperations;

import java.util.ArrayList;
import java.util.Collection;

import fileProcessorDecorator.util.FileDisplayInterface;
import fileProcessorDecorator.util.Results;

/**
 * @author prathamesh
 *
 */
public class WordDecorator extends FileProcessorAbstractBase{
	private ArrayList<String> sentDecorator;
	private ArrayList<String> wordDecorator;
	private FileProcessorAbstractBase baseClass;
	private FileDisplayInterface results;

	public WordDecorator(FileProcessorAbstractBase baseClassIn, FileDisplayInterface resIn) {
		// TODO Auto-generated constructor stub
		baseClass = baseClassIn;		
		results = resIn;
	}
	@Override
	public Collection getDecorator(InputDetails ip) {
		sentDecorator =  (ArrayList<String>) baseClass.getDecorator(ip);
		wordDecorator = ip.getResWord();
		for(String s : sentDecorator){
			String[] chopped = s.split("\\s+");
			for(int i = 0; i < chopped.length; i++){
				String temp =  chopped[i];
				if(temp.endsWith(".")){
					temp = temp.substring(0, temp.length() -1);
				}
				if(!temp.equalsIgnoreCase("")){	
					wordDecorator.add(temp);
				}
				
			}
		}
		((Results)results).storeNewResult("\n---DECORATOR_WordDecorator_START --- ") ;

		for(String s : wordDecorator){
			((Results)results).storeNewResult(s) ;
		}
		((Results)results).storeNewResult("---DECORATOR_WordDecorator_END --- ") ;

		return wordDecorator;
		
	}


}
