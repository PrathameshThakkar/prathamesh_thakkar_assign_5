/**
 * 
 */
package fileProcessorDecorator.fileOperations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author prathamesh
 *
 */
public class InputDetails {
	private String filename;
	private ArrayList<String>resParagraph;
	private ArrayList<String>resLine;
	private ArrayList<String>resWord;
	private ArrayList<String>resInitialStorage;
	private Map<String, Integer> resWordFrequency;
	
	/**
	 * @return reference to wordFrequency HashMap
	 */
	public Map<String, Integer> getResWordFrequency() {
		return resWordFrequency;
	}

	/**
	 * @return reference to resParagraph ArrayList
	 */
	public ArrayList<String> getResParagraph() {
		return resParagraph;
	}
	
	/**
	 * @return reference to resLine ArrayList
	 */
	public ArrayList<String> getResLine() {
		return resLine;
	}
	
	/**
	 * @return reference to resword ArrayList
	 */
	public ArrayList<String> getResWord() {
		return resWord;
	}
	
	/**
	 * @return reference to resInitialStorage ArrayList
	 */
	public ArrayList<String> getResInitialStorage() {
		return resInitialStorage;
	}
	
	public InputDetails(){
		//filename = filenameIn;
		resParagraph = new ArrayList<String>();
		resLine = new ArrayList<String>();
		resWord = new ArrayList<String>();
		resInitialStorage = new ArrayList<String>();
		resWordFrequency = new HashMap<String, Integer>();
	}

}
