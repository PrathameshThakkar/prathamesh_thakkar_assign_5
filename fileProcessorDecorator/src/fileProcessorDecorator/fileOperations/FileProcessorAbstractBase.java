/**
 * 
 */
package fileProcessorDecorator.fileOperations;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author prathamesh
 *
 */
public abstract class FileProcessorAbstractBase {
	public abstract Collection getDecorator(InputDetails InputDetailsIn);
}
