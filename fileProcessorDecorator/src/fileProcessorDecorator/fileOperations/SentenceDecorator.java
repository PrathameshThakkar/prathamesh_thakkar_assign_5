/**
 * 
 */
package fileProcessorDecorator.fileOperations;

import java.util.ArrayList;
import java.util.Collection;

import fileProcessorDecorator.util.FileDisplayInterface;
import fileProcessorDecorator.util.Results;

/**
 * @author prathamesh
 *
 */
public class SentenceDecorator extends FileProcessorAbstractBase {
	private ArrayList<String> paraDecorator;
	private ArrayList<String> sentDecorator;
	private FileProcessorAbstractBase baseClass;
	private FileDisplayInterface results; 
	public SentenceDecorator(FileProcessorAbstractBase baseClassIn, FileDisplayInterface resIn){
		baseClass = baseClassIn;
		results = resIn;
	}
	@Override
	public Collection getDecorator(InputDetails ip) {

		paraDecorator =  (ArrayList<String>) baseClass.getDecorator(ip);
		sentDecorator = ip.getResLine();
		StringBuilder sb = new StringBuilder();
		for(String temp : paraDecorator){
			sb.append(temp);
		}	
		String[] seperate = sb.toString().split("(?<=\\.)");
		
		for(String temp : seperate){
			sentDecorator.add(temp);
		}
		((Results)results).storeNewResult("") ;

		((Results)results).storeNewResult("---DECORATOR_SentenceDecorator_START --- ") ;

		for(String s : sentDecorator){
			((Results)results).storeNewResult(s) ;
		}
		((Results)results).storeNewResult("---DECORATOR_SentenceDecorator_END --- ") ;

		return sentDecorator;
	}

}


