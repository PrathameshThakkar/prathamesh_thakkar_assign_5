/**
 * 
 */
package fileProcessorDecorator.util;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * @author prathamesh
 *
 */
public class Results implements FileDisplayInterface, StdoutDisplayInterface {
	private String filename;
	private ArrayList<String> testResults = new ArrayList<String>();


	@Override
	public void writeToFile(String filenameIn) {
		// TODO Auto-generated method stub
		filename = filenameIn;
		PrintWriter out = null;
		try {
			out = new PrintWriter(filename);
			for(String res : testResults){
				out.println(res);
			}
		} catch (FileNotFoundException e) {
			System.err.print("File " + filename + "not found");
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} finally{
			if(out != null) {out.close();}
		}
	}

	@Override
	public void writeToStdout() {
		// TODO Auto-generated method stub
		
	}
	/**
	 * 
	 * @param res, string to be stored in array list 
	 */
	public void storeNewResult(String res){
		testResults.add(res);
		}
	

}
