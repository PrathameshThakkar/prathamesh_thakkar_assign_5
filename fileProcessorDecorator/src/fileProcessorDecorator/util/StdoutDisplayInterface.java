/**
 * 
 */
package fileProcessorDecorator.util;

/**
 * @author prathamesh
 *
 */
public interface StdoutDisplayInterface {
	void writeToStdout();
}
