/**
 * 
 */
package fileProcessorDecorator.util;

/**
 * @author prathamesh
 *
 */
public interface FileDisplayInterface {
	void writeToFile(String S);
}
