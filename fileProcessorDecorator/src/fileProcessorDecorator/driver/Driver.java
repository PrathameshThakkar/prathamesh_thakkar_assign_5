/**
 * 
 */
package fileProcessorDecorator.driver;

import fileProcessorDecorator.fileOperations.FileProcessorAbstractBase;
import fileProcessorDecorator.fileOperations.FileRead;
import fileProcessorDecorator.fileOperations.InputDetails;
import fileProcessorDecorator.fileOperations.ParagraphDecorator;
import fileProcessorDecorator.fileOperations.SentenceDecorator;
import fileProcessorDecorator.fileOperations.WordDecorator;
import fileProcessorDecorator.fileOperations.WordFrequencyDecorator;
import fileProcessorDecorator.util.FileDisplayInterface;
import fileProcessorDecorator.util.Results;

/**
 * @author prathamesh
 *
 */
public class Driver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String inputFile = null;
		String outputFile = null;
		if(args.length != 2){
			System.err.println("Expected two arguments");
			System.exit(1);
		}
		else{
			inputFile = args[0];
			outputFile = args[1];
		}
		FileDisplayInterface res = new Results();
		InputDetails ip = new InputDetails();
		FileRead read = new FileRead(inputFile, ip);
		read.initialStorage();
		//ip.tempDisplay();
		FileProcessorAbstractBase initial = null;
		FileProcessorAbstractBase base = new ParagraphDecorator(initial, res);
		FileProcessorAbstractBase base1 = new SentenceDecorator(base, res);
		FileProcessorAbstractBase base2 = new WordDecorator(base1, res);
		FileProcessorAbstractBase base3 = new WordFrequencyDecorator(base2, res);
		base3.getDecorator(ip);
		res.writeToFile(outputFile);
		
	}
}
